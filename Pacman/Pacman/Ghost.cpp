#include "Ghost.h"


Ghost::Ghost() :MobileElement() {
	color = 0;
	ghostStatus = GhostStatus::TRACK;
}

Ghost::Ghost(int _color, GhostStatus _ghostStatus) : MobileElement() {
	color = _color;
	ghostStatus = _ghostStatus;
}

Ghost::Ghost(int x, int y, Direction direction, int speed, int position, int statusTime, int _color, GhostStatus _ghostStatus) : MobileElement(x,y,direction,speed, position, statusTime) {
	color = _color;
	ghostStatus = _ghostStatus;
}

GhostStatus Ghost::getGhostStatus() {
	return ghostStatus;
}
int Ghost::getColor() {
	return color;
}

void Ghost::SetGhostStatus(GhostStatus _ghostStatus) {
	ghostStatus = _ghostStatus;
}
void Ghost::SetColor(int _color) {
	color = _color;
}
