#include "MobileElement.h"


MobileElement::MobileElement():Element() {
	direction = Direction::NONE;
	speed = 0;
	position = 0;
	statusTime = 0;
}

MobileElement::MobileElement(Direction _direction, int _speed, int _position, int _statusTime):Element() {
	direction = _direction;
	speed = _speed;
	position = _position;
	statusTime = _statusTime;
}

MobileElement::MobileElement(int _x, int _y, Direction _direction, int _speed, int _position, int _statusTime):Element(_x,_y) {
	direction = _direction;
	speed = _speed;
	position = _position;
	statusTime = _statusTime;
}

Direction MobileElement::GetDirection() {
	return direction;
}

int MobileElement::GetPosition() {
	return position;
}

int MobileElement::GetSpeed() {
	return speed;
}

int MobileElement::GetStatusTime() {
	return statusTime;
}

void MobileElement::SetDirection(Direction _dir) {
	direction = _dir;
}

void MobileElement::SetPosition(int _pos) {
	position = _pos;
}

void MobileElement::SetSpeed(int _speed) { speed = _speed; }

void MobileElement::SetStatusTime(int _stat) {
	statusTime = _stat;
}
