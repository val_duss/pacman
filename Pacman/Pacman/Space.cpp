#include "Space.h"

Space::Space() :StaticElement() {
	spaceTypeId = SpaceTypeId::EMPTY;
}

Space::Space(SpaceTypeId _spaceTypeId) : StaticElement() {
	spaceTypeId = _spaceTypeId;
}

Space::Space(int x, int y, SpaceTypeId _spaceTypeId) : StaticElement(x,y) {
	spaceTypeId = _spaceTypeId;
}

SpaceTypeId Space::GetspaceTypeId() {
	return spaceTypeId;
}

void Space::SetspaceTypeId(SpaceTypeId _spaceTypeId) {
	spaceTypeId = _spaceTypeId;
}