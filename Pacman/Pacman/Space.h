#pragma once
#include "StaticElement.h"

enum class SpaceTypeId { EMPTY, GUM, SUPERGUM, GRAVEYARD, START };

class Space : public StaticElement
{
private:
    SpaceTypeId spaceTypeId;
public :
    ~Space();
    Space();
    Space(SpaceTypeId spaceTypeId);
    Space(int x, int y, SpaceTypeId spaceTypeId);
    SpaceTypeId GetspaceTypeId();
    void SetspaceTypeId(SpaceTypeId spaceTypeId);

};

