#pragma once
#include "Element.h"
 
class StaticElement : public Element
{
public :
    ~StaticElement();
    StaticElement();
    StaticElement(int x, int y);

};

