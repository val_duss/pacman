#include "Pacman.h"

Pacman::Pacman() : MobileElement() {
	status = PacmanStatus::NORMAL;
}


Pacman::Pacman(PacmanStatus _status) : MobileElement() {
	status = _status;
}

Pacman::Pacman(int x, int y, Direction direction, int speed,int position, int statusTime, PacmanStatus _status) : MobileElement(x, y, direction, speed, position, statusTime) {
	status = _status;
}


PacmanStatus Pacman::GetStatus() {
	return status;
}
void Pacman::SetStatus(PacmanStatus _status) {
	status = _status;
}