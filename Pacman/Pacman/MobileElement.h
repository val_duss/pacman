#pragma once
#include "Element.h"
enum class Direction { NONE, NORTH, SOUTH, EAST, WEST };

class MobileElement : public Element
{
private:
	Direction direction;
	int speed;
	int position;
	int statusTime;
public:
	~MobileElement();
	MobileElement();
	MobileElement(Direction direction, int speed, int position, int statusTime);
	MobileElement(int x, int y,Direction direction, int speed, int position, int statusTime);
	Direction GetDirection();
	int GetSpeed();
	int GetPosition();
	int GetStatusTime();

	void SetDirection(Direction direction);
	void SetSpeed(int speed);
	void SetPosition(int position);
	void SetStatusTime(int statusTime);
};

