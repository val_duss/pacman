#include "Wall.h"

Wall::Wall() :StaticElement() {
	wallTypeId = WallTypeId::VERTICAL;
}

Wall::Wall(WallTypeId _wallTypeId) : StaticElement() {
	wallTypeId = _wallTypeId;
}

Wall::Wall(int x, int y, WallTypeId _wallTypeId) : StaticElement(x,y) {
	wallTypeId = _wallTypeId;
}

WallTypeId Wall::GetwallTypeId() {
	return wallTypeId;
}
void Wall::SetwallTypeId(WallTypeId _wallTypeId) {
	wallTypeId = _wallTypeId;
}