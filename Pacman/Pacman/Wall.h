#pragma once
#include "StaticElement.h"
enum class WallTypeId { TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, HORIZONTAL, VERTICAL };

class Wall : public StaticElement
{
	private :
		WallTypeId wallTypeId;
	public :
		~Wall();
		Wall();
		Wall(WallTypeId wallTypeId);
		Wall(int x, int y,WallTypeId wallTypeId);


		WallTypeId GetwallTypeId();
		void SetwallTypeId(WallTypeId wallTypeId);

};

