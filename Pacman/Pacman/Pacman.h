#pragma once
#include "MobileElement.h"
enum class PacmanStatus { NORMAL, SUPER, DEAD };

class Pacman :public MobileElement
{
public:
	~Pacman();
	Pacman();
	Pacman(PacmanStatus status);
	Pacman(int x, int y, Direction direction, int speed,int position, int statusTime, PacmanStatus status);
	PacmanStatus GetStatus();
	void SetStatus(PacmanStatus status);
private:
	PacmanStatus status;

};

