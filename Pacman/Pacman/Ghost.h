#pragma once
#include "MobileElement.h"
enum class GhostStatus { TRACK, EYES, FLEE };

class Ghost : public MobileElement
{
private:
	int color;
	GhostStatus ghostStatus;
public:
	~Ghost();
	Ghost();
	Ghost(int color, GhostStatus ghostStatus);
	Ghost(int x, int y, Direction direction, int speed, int position, int statusTime, int color, GhostStatus ghostStatus);

	GhostStatus getGhostStatus();
	int getColor();

	void SetGhostStatus(GhostStatus ghostStatus);
	void SetColor(int color);
};

